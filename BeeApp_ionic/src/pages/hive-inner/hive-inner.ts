import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'hive-inner',
  templateUrl: 'hive-inner.html'
})
export class HiveInner {

  constructor(public navCtrl: NavController) {
  
    }

}
