import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { HiveInner } from '../hive-inner/hive-inner';


@Component({
  selector: 'page-hives',
  templateUrl: 'hives.html'
})
export class HivesPage {

  constructor(public navCtrl: NavController) {
  }

  openHivePage() {
      this.navCtrl.push(HiveInner);
  }

}
