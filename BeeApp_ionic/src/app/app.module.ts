import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HivesPage } from '../pages/hives/hives';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { HiveInner } from '../pages/hive-inner/hive-inner';

@NgModule({
  declarations: [
    MyApp,
    HivesPage,
    ContactPage,
    HomePage,
    TabsPage,
    HiveInner
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HivesPage,
    ContactPage,
    HomePage,
    TabsPage,
    HiveInner
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
